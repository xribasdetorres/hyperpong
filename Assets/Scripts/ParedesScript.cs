﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParedesScript : MonoBehaviour
{

    
    public int Potencia;
    

    private Rigidbody rgbdPelota;
    private GameObject pelota;
    private float lastcontx;

    // Start is called before the first frame update
    void Start()
    {
        
        pelota = GameObject.FindGameObjectWithTag("Pelota");
        rgbdPelota = pelota.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(pelota == null)
        {
            pelota = GameObject.FindGameObjectWithTag("Pelota");
            rgbdPelota = pelota.GetComponent<Rigidbody>();
        }
        
    }

    void OnCollisionEnter(Collision col)
    {

        if (col.collider.tag == "Pelota")
        {

            foreach (ContactPoint contact in col.contacts)
            {
               
                
              rgbdPelota.AddForce(-2 * contact.normal.x * Potencia, -1 * contact.normal.y * Potencia, 0, ForceMode.Impulse);
                

            }


        }

    }
}
