﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ControllerSelector : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Arrows;
    [SerializeField]
    private TextMeshProUGUI ControllerText;

    private bool Selected;
    private bool pressJL;
    private bool pressJR;
    ArrayList Controller = new ArrayList();
    private string[] ControllerConf = new string[3];
    private int Joystics;
    private int indexController;
    private string actualController;
    private bool actual = false;
    private bool detectJoy1 = false;
    private string joy1 = "";
    private string joy2 = "";

    // Start is called before the first frame update
    void Start()
    {
        pressJL = false;
        pressJR = false;
        indexController = 0;
        ControllerConf[0] = "keyboard";
        Controller = getControllerList(GetNumJoys());
        actualController = Controller[0].ToString();
        ControllerText.text = actualController;
        Joystics = GetNumJoys();
        
       
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Joystics != GetNumJoys())
        {
            Joystics = GetNumJoys();
            Controller = getControllerList(GetNumJoys());

        }
        
        
        if (Selected)
        {
            if (InputController.Instance.getAxisXJoystic() > 0)
            {
                if (pressJR == false)
                {
                    pressJR = true;
                    if(indexController < Controller.Count - 1)
                    {

                       
                            ++indexController;
                            actualController = Controller[indexController].ToString();
                            ControllerText.text = actualController;
                        

                    }
                }
            }
            if (InputController.Instance.getKeyRight())
            {
                if (indexController < Controller.Count - 1)
                {
                   
                        ++indexController;
                    actualController = Controller[indexController].ToString();
                    ControllerText.text = actualController;
                    
                }
            }
            if (InputController.Instance.getAxisXJoystic() < 0)
            {
                if (pressJL == false)
                {
                    pressJL = true;
                    if (indexController > 0)
                    {
                        --indexController;
                        actualController = Controller[indexController].ToString();
                        ControllerText.text = actualController;
                    }
                }
            }
            if (InputController.Instance.getKeyLeft())
            {
                if (indexController > 0)
                {
                    --indexController;
                    actualController = Controller[indexController].ToString();
                    ControllerText.text = actualController;
                }
            }
            if (InputController.Instance.getAxisXJoystic() == 0)
            {
                pressJL = false;
                pressJR = false;
            }
        }
        if(Joystics > 0)
        {

            if (indexController == 0)
            {
                Arrows[1].SetActive(false);
            }
            else
            {
                Arrows[1].SetActive(true);
            }
            if (indexController == Controller.Count - 1)
            {
                Arrows[0].SetActive(false);
            }
            else
            {
                Arrows[0].SetActive(true);
            }
        }
        else
        {
            Arrows[0].SetActive(false);
            Arrows[1].SetActive(false);
        }

        
        


        if(Input.GetKeyDown(KeyCode.G))
        {
            getController1();
        }
    }
    public void SetSelected()
    {
        if (Selected == false)
        {
            Selected = true;
        }
        else
        {
            Selected = false;
        }
    }

    public string getController1()
    {
        return ControllerConf[indexController];
    }

    public string getController2()
    {
        return Controller[indexController].ToString();
    }

   
    public int GetNumJoys()
    {
        int numJoys = 0;
        for (int x = 0; x < Input.GetJoystickNames().Length; x++)
        {
            if (Input.GetJoystickNames()[x] != null && Input.GetJoystickNames()[x] != "" && Input.GetJoystickNames()[x] != " ")
            {
                numJoys++;
            }

        }
        return numJoys;
    }
    public ArrayList getControllerList(int nJoys)
    {
        ArrayList cArray = new ArrayList();

        cArray.Add("keyboard");

        if (nJoys > 0)
        {
            for (int x = 0; x < nJoys; x++)
            {
                if (Input.GetJoystickNames().Length >= 2)
                {
                    if (x == 1)
                    {
                        cArray.Add("joystic");
                    }
                    else if (x != 0 && x != 1)
                    {
                        cArray.Add($"joystic{x - 1}");
                    }
                }
                else
                {
                    cArray.Add("joystic");
                }
            }
        }


        return cArray;
    }


}
