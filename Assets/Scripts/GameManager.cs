﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    //Variables de debug;
    private enum modeDebug { ON, OFF};
   [SerializeField] private modeDebug mDebug = modeDebug.OFF;


    //Variables funcionamento juego
    [SerializeField] GameObject PelInstance;
    [SerializeField] GameObject CajaPowerUp;
    [SerializeField] public int WinPoints;
   
    private DificSelection IAdificultat;
    private GameObject InitObject;
    private InitSingleton InitScript;

    private GameObject P1;
    private GameObject P2;
    private GameObject PlayerEnemy;
    private GameObject Player;


    private Rigidbody RgPlayer;
    private Rigidbody RgEnemy;

    private IAEnemy IAPlayer2;

    private TextMeshProUGUI Player1P;

    private TextMeshProUGUI Player2P;


    private GameObject Pelota;
    private Vector2 FuerzaPelota;
    private GameObject Panel1;
    private GameObject Panel2;
    private GameObject PanelPause;
    private GameObject PanelOptions;

    private Rigidbody rgbPelota;
    private Vector3 PosInPlayer;
    private Vector3 PosInEnemy;

    //Variables del juego
    private int Players;
    private Vector3 PositionInitial;
    private Quaternion normal;
    private int direccion;
    private int random;
    private int puntuacionJug1;
    private int puntuacionJug2;
    private TipoPowerUps tipoPowerUpCaja;
    private int randomTipoCaja;
    private Vector3 PosCaja;
    private float tiempoGenPU;
    [SerializeField]
    private int maxPU = 3;
    private int numPU;
    private GameObject OptionsMenu;


    public void StartGame()
    {

        OptionsMenu = GameObject.Find("OptionsMenu");
        InitObject = GameObject.Find("Manager");
        InitScript = InitObject.GetComponent<InitSingleton>();
        InitScript.StartMusic();
        P1 = GameObject.Find("Player1Points");
        P2 = GameObject.Find("Player2Points");
        Player1P = P1.GetComponent<TextMeshProUGUI>();
        Player2P = P2.GetComponent<TextMeshProUGUI>();
        PlayerEnemy = GameObject.Find("PlayerEnemy");
        Player = GameObject.Find("Pala");
        Panel1 = GameObject.Find("Player1Win");
        Panel2 = GameObject.Find("Player2Win");
        PanelPause = GameObject.Find("PauseMenu");
        PanelOptions = GameObject.Find("OptionsMenu");
        IAPlayer2 = PlayerEnemy.GetComponent<IAEnemy>();

        
        OptionsMenu.SetActive(false);

        PosInPlayer = Player.transform.position;
        PosInEnemy = PlayerEnemy.transform.position;

        RgPlayer = Player.GetComponent<Rigidbody>();

        if(IAdificultat == DificSelection.Facil)
        {
            IAPlayer2.setDificult("Facil");
        }
        else if(IAdificultat == DificSelection.Normal)
        {
            IAPlayer2.setDificult("Normal");
        }
        else
        {
            IAPlayer2.setDificult("Dificil");
        }

        normal.Set(0, 0, 0, 0);
        Pelota = GameObject.FindGameObjectWithTag("Pelota");
        PositionInitial = Pelota.transform.position;
        random = Random.Range(-1, 1);
        if(random == 0)
        {
            random = 1;
        }
        FuerzaPelota.Set(random * 20, 3);
        rgbPelota = Pelota.GetComponent<Rigidbody>();
        RgPlayer.isKinematic = false;
        puntuacionJug1 = 0;
        puntuacionJug2 = 0;
        Time.timeScale = 1;

        rgbPelota.AddForce(FuerzaPelota, ForceMode.Impulse);

        Panel1.SetActive(false);
        Panel2.SetActive(false);
        PanelPause.SetActive(false);
        numPU = 0;
        
    }

    public void StartGameM()
    {

        OptionsMenu = GameObject.Find("OptionsMenu");
        InitObject = GameObject.Find("Manager");
        InitScript = InitObject.GetComponent<InitSingleton>();
        InitScript.StartMusic();
        P1 = GameObject.Find("Player1Points");
        P2 = GameObject.Find("Player2Points");
        Player1P = P1.GetComponent<TextMeshProUGUI>();
        Player2P = P2.GetComponent<TextMeshProUGUI>();
        PlayerEnemy = GameObject.Find("Player2");
        Player = GameObject.Find("Pala");
        Panel1 = GameObject.Find("Player1Win");
        Panel2 = GameObject.Find("Player2Win");
        PanelPause = GameObject.Find("PauseMenu");
        PanelOptions = GameObject.Find("OptionsMenu");
        IAPlayer2 = PlayerEnemy.GetComponent<IAEnemy>();


        OptionsMenu.SetActive(false);

        PosInPlayer = Player.transform.position;
        PosInEnemy = PlayerEnemy.transform.position;

        RgPlayer = Player.GetComponent<Rigidbody>();
        RgEnemy = PlayerEnemy.GetComponent<Rigidbody>();
       

        normal.Set(0, 0, 0, 0);
        Pelota = GameObject.FindGameObjectWithTag("Pelota");
        PositionInitial = Pelota.transform.position;
        random = Random.Range(-1, 1);
        if (random == 0)
        {
            random = 1;
        }
        FuerzaPelota.Set(random * 20, 3);
        rgbPelota = Pelota.GetComponent<Rigidbody>();
        RgPlayer.isKinematic = false;
        RgEnemy.isKinematic = false;
        puntuacionJug1 = 0;
        puntuacionJug2 = 0;
        Time.timeScale = 1;

        rgbPelota.AddForce(FuerzaPelota, ForceMode.Impulse);

        Panel1.SetActive(false);
        Panel2.SetActive(false);
        PanelPause.SetActive(false);
        numPU = 0;

    }

    // Start is called before the first frame update
    void Start()
    {
        

    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "_Carga")
        {
            SceneManager.LoadScene("Juego");
        }
        if (SceneManager.GetActiveScene().name == "Juego" || SceneManager.GetActiveScene().name == "Multiplayer")
        {
            

            if (Pelota == null)
            {
                GameObject.Instantiate(PelInstance, PositionInitial, normal);
                Pelota = GameObject.FindGameObjectWithTag("Pelota");
                rgbPelota = Pelota.GetComponent<Rigidbody>();
                random = Random.Range(-1, 1);
                if (random == 0)
                {
                    random = 1;
                }
                FuerzaPelota.Set(random * 20, 3);
                rgbPelota.AddForce(FuerzaPelota, ForceMode.Impulse);
            }
            Player1P.text = puntuacionJug1.ToString();
            Player2P.text = puntuacionJug2.ToString();

            if(puntuacionJug1 >= WinPoints)
            {
                Cursor.visible = true;
                Time.timeScale = 0;
                RgPlayer.isKinematic = true;
                if (SceneManager.GetActiveScene().name == "Multiplayer")
                {
                    RgEnemy.isKinematic = true;
                }
                Panel1.SetActive(true);

            }
            if (puntuacionJug2 >= WinPoints)
            {
                Cursor.visible = true;
                Time.timeScale = 0;
                RgPlayer.isKinematic = true;
                if(SceneManager.GetActiveScene().name == "Multiplayer")
                {
                    RgEnemy.isKinematic = true;
                }
                Panel2.SetActive(true);
            }
            tiempoGenPU += Time.deltaTime;
            if (tiempoGenPU >= 3)
            {
                tiempoGenPU = 0;
                if (numPU <  maxPU)
                {
                    genPU();
                }
            }
            if (mDebug == modeDebug.ON)
            {
                if (InputController.Instance.GetCreatePowerUP())
                {
                    ++numPU;
                    randomTipoCaja = Random.Range(0, 2);
                    switch (randomTipoCaja)
                    {
                        case 0:
                            tipoPowerUpCaja = TipoPowerUps.Rebote;
                            break;
                        case 1:
                            tipoPowerUpCaja = TipoPowerUps.MasPotencia;
                            break;
                        case 2:
                            tipoPowerUpCaja = TipoPowerUps.Tipo3;
                            break;
                        default:
                            Debug.Log("Random de tipo caja: " + randomTipoCaja);
                            tipoPowerUpCaja = TipoPowerUps.Rebote;
                            break;
                    }
                    float posyCajaPowerup = Random.Range(-7f, 7f);
                    float velCajaPU = Random.Range(-100, 100);
                    PosCaja.Set(0, posyCajaPowerup, -5.16f);
                    GameObject CajaPU = Instantiate(CajaPowerUp, PosCaja, normal);
                    CajaPU.GetComponent<CajaPowerUp>().TipoPowerUp = tipoPowerUpCaja;
                    CajaPU.GetComponent<Rigidbody>().AddForce(new Vector3(-100, 0, 0), ForceMode.Force);
                }
            }

        if(GameObject.Find("(singleton) InputController") || GameObject.Find("InputController"))
        {
            if (InputController.Instance.GetControler() == "joystick")
            {
                if (InputController.Instance.getStartButton())
                {
                    PauseMenu();
                }
            }
            else
            {

                if (InputController.Instance.getEscButton())
                {

                    PauseMenu();
                }
                    
            }
        }
        
        }
        
        
    }
    public void AddPuntacionJug1()
    {
        ++puntuacionJug1;
    }
    public void AddPuntacionJug2()
    {
        ++puntuacionJug2;
    }
    private void PauseMenu()
    {   
        Time.timeScale = 0;
        Player.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        Player.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        Cursor.visible = true;
        //RgEnemy.isKinematic = true;
        //RgPlayer.isKinematic = true;
        PanelPause.SetActive(true);
    }
    public void ResumeGame()
    {
        PanelPause.SetActive(false);
        Cursor.visible = false;
        Time.timeScale = 1;
        //RgEnemy.isKinematic = false;
        //RgPlayer.isKinematic = false;
    }
    public void RestartGame()
    {
        
    }
    public void Gotomenu()
    {
        SceneManager.LoadScene("Menu");
    }
    public void OpenOptions()
    {
        PanelOptions.SetActive(true);

    }
    public void setGame(DificSelection difficultat, int points)
    {
        IAdificultat = difficultat;

        WinPoints = points;
    }

    public void setMGame(int Points)
    {
        WinPoints = Points;
    }
    public void getPU()
    {
        numPU -= 1;
    }
    private void genPU()
    {
        ++numPU;
        randomTipoCaja = Random.Range(0, 2);
        switch (randomTipoCaja)
        {
            case 0:
                tipoPowerUpCaja = TipoPowerUps.Rebote;
                break;
            case 1:
                tipoPowerUpCaja = TipoPowerUps.MasPotencia;
                break;
            //case 2:
            //    tipoPowerUpCaja = TipoPowerUps.Tipo3;
            //    break;
            default:
                Debug.Log("Random de tipo caja: " + randomTipoCaja);
                tipoPowerUpCaja = TipoPowerUps.Rebote;
                break;
        }
        float posyCajaPowerup = Random.Range(-7f, 7f);
        float velocCaja = Random.Range(-100, 100);
        PosCaja.Set(0, posyCajaPowerup, -5.16f);
        GameObject CajaPU = Instantiate(CajaPowerUp, PosCaja, normal);
        CajaPU.GetComponent<CajaPowerUp>().TipoPowerUp = tipoPowerUpCaja;
        CajaPU.GetComponent<Rigidbody>().AddForce(new Vector3(velocCaja, 0, 0), ForceMode.Force);
    }
    public int getPointsP1()
    {
        return puntuacionJug1;
    }
    public int getPointsEnemy()
    {
        return puntuacionJug2;
    }
}
