﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum estadosPowers
{
    Disponibe,
    NoDisponible,
    Activado
};
public class PlayerController : MonoBehaviour
{
    private enum Sensibilidad
    {
        SENCILLA,
        RAPIDA,
        LENTA

    };

    
    [SerializeField] Sensibilidad Sensi;


    public string ControllerPlayer1;
    public int Potencia;
    [Range(0, 60)]
    public float timePowers;

    private estadosPowers estadoPower1;
    private estadosPowers estadoPower2;
    private Rigidbody rgbPalaPlayer;
    private string controller;
    private double Move;
    private Vector3 vectormove;
    private Rigidbody rgbdPelota;
    private GameObject Pelota;
    private float currentTimePower1;
    private float currentTimePower2;
    private bool Power1Act;
    private bool Power2Act;
    private AudioSource bumppala;
    private int SensiController;

    void Awake()
    {
        if (GameObject.Find("InputController") == null)
        {
            Debug.Log("No encontrado el Input");
            return;
        }

      
    }

    void Start()
    {
        SensiController = Config.Instance.Sensi;
        estadoPower1 = estadosPowers.NoDisponible;
        estadoPower2 = estadosPowers.NoDisponible;
        Pelota = GameObject.FindGameObjectWithTag("Pelota");
        rgbdPelota = Pelota.GetComponent<Rigidbody>();
        rgbPalaPlayer = GetComponent<Rigidbody>();
        controller = InputController.Instance.GetControler();
        vectormove = new Vector3(0, 0, 0);
        bumppala = GetComponent<AudioSource>();
        SetSensi(SensiController);
    }

    // Update is called once per frame
    void Update()
    {
        if (Pelota == null)
        {
            Pelota = GameObject.FindGameObjectWithTag("Pelota");
            rgbdPelota = Pelota.GetComponent<Rigidbody>();
            
        }

        if(SceneManager.GetActiveScene().name == "Juego")
        { 
            controller = InputController.Instance.GetControler();
        }
        if (SceneManager.GetActiveScene().name == "Multiplayer")
        {
            controller = Config.Instance.getControllerPlayer1();
            if (controller == "keyboard")
            {
                SetSensi(Config.Instance.getSensiPlayer1());
            }

        }
        if (controller == "keyboard")
        {
            if(Sensi == Sensibilidad.SENCILLA)
            {
                Move = InputController.Instance.getAxisYKeyboard() * 0.25;
            }
            if (Sensi == Sensibilidad.LENTA)
            {
                Move = InputController.Instance.getAxisYKeyboard() * 0.1;
            }
            if(Sensi == Sensibilidad.RAPIDA)
            {
                Move = InputController.Instance.getAxisYKeyboard();
            }
        }
        if (controller == "joystic")
        {
            Move = InputController.Instance.getAxisYJoystic() * 0.25;
            if (InputController.Instance.GetPower1JoyButton())
            {

                if (estadoPower1 == estadosPowers.Disponibe)
                {
                    currentTimePower1 = timePowers;
                    Power1Act = true;
                    estadoPower1 = estadosPowers.Activado;
                }
            }
            if (InputController.Instance.GetPower2JoyButton())
            {

                if (estadoPower2 == estadosPowers.Disponibe)
                {
                    currentTimePower2 = timePowers;
                    Power2Act = true;
                    estadoPower2 = estadosPowers.Activado;
                    Potencia = Potencia * 2;
                }
            }
        }
        if (controller == "joystic1")
        {
            Move = InputController.Instance.getAxisYJoystic1() * 0.25;
            if (InputController.Instance.GetPower1Joy1Button())
            {

                if (estadoPower1 == estadosPowers.Disponibe)
                {
                    currentTimePower1 = timePowers;
                    Power1Act = true;
                    estadoPower1 = estadosPowers.Activado;
                }
            }
            if (InputController.Instance.GetPower2Joy1Button())
            {

                if (estadoPower2 == estadosPowers.Disponibe)
                {
                    currentTimePower2 = timePowers;
                    Power2Act = true;
                    estadoPower2 = estadosPowers.Activado;
                    Potencia = Potencia * 2;
                }
            }
        }
        if (controller == "keyboard")
        {
            if(InputController.Instance.GetPower1KeyButton())
            {
                
                if(estadoPower1 == estadosPowers.Disponibe)
                {
                    currentTimePower1 = timePowers;
                    Power1Act = true;
                    estadoPower1 = estadosPowers.Activado;
                }
            }
            if(InputController.Instance.GetPower2KeyButton())
            {
                if (estadoPower2 == estadosPowers.Disponibe)
                {
                    currentTimePower2 = timePowers;
                    Power2Act = true;
                    estadoPower2 = estadosPowers.Activado;
                    Potencia = Potencia + 10;
                }
            }
        }
        vectormove.Set(0, 0, (float)-Move);
        if(Move == 0)
        {
            transform.GetComponent<Rigidbody>().velocity.Set(0,0,0);
        }
        transform.Translate(vectormove);
        if(estadoPower1 == estadosPowers.Activado)
        {
            currentTimePower1 -= Time.deltaTime;
            
        }
        if (estadoPower1 == estadosPowers.Activado && currentTimePower1 <= 0)
        {
            Power1Act = false;
            estadoPower1 = estadosPowers.NoDisponible;
            currentTimePower1 = 0;
        }
        if (estadoPower2 == estadosPowers.Activado)
        {
            currentTimePower2 -= Time.deltaTime;

        }
        if (estadoPower2 == estadosPowers.Activado && currentTimePower2 <= 0)
        {
            Power2Act = false;
            estadoPower2 = estadosPowers.NoDisponible;
            currentTimePower2 = 0;
            Potencia = Potencia - 10;
        }
    }

    void OnCollisionEnter(Collision col)
    {
        bumppala.Play();
        if (col.collider.tag == "Pelota")
        {
            gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
            foreach (ContactPoint contact in col.contacts)
            {

                rgbdPelota.AddForce(-1 * contact.normal.x * Potencia, -1 * contact.normal.y * Potencia, 0, ForceMode.Impulse);

            }


        }
       

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PowerUp")
        {

                TipoPowerUps tipoCaja = col.gameObject.GetComponent<CajaPowerUp>().TipoPowerUp;
                switch (tipoCaja)
                {
                    case TipoPowerUps.Rebote:
                        if(estadoPower1 == estadosPowers.NoDisponible)
                        {
                        estadoPower1 = estadosPowers.Disponibe;
                        }
                        break;
                    case TipoPowerUps.MasPotencia:
                        if (estadoPower2 == estadosPowers.NoDisponible)
                        {
                            estadoPower2 = estadosPowers.Disponibe;
                        }
                        break;
                    default:
                        Debug.Log(tipoCaja);
                        break;
                }
            
           Destroy(col.gameObject);
        }
    }

    public void SetSensi(int sensiValue)
    {
        switch(sensiValue)
        {
            case 1:
            Sensi = Sensibilidad.LENTA;
            break;
            case 2:
            Sensi = Sensibilidad.SENCILLA;
            break;
            case 3:
            Sensi = Sensibilidad.RAPIDA;
            break;
            default: break;

        }
    }
    public float getTime()
    {
        return currentTimePower1;
    }

    public float getTime2()
    {
        return currentTimePower2;
    }

    public bool getActivated()
    {
        return Power1Act;
    }

    public bool getActivated2()
    {
        return Power2Act;
    }
    public estadosPowers getEstadoPower1()
    {
        return estadoPower1;
    }
    public estadosPowers getEstadoPower2()
    {
        return estadoPower2;
    }
}
