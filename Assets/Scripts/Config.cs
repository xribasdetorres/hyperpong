﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System;

[RequireComponent(typeof(AudioMixer))]

public class Config : Singleton<Config>
{
    public Dropdown DropController;
    public Dropdown DropSensi;
    public Dropdown DropControllerPlayer2;
    public Dropdown DropSensiPlayer2;
    public int Sensi = 1;
    public float soundVolume = 5.0f;
    public float musicVolume = 5.0f;
    public TextMeshProUGUI textoprueba;

    private AudioMixer mixer;

    private Button btnClose;
    private Slider SoundSlider;
    private Slider MusicSlider;
    private TextMeshProUGUI soundTxt;
    private TextMeshProUGUI musicTxt;
    private GameObject ControllerPlayer2;
    private GameObject SensiPlayer2;
    private GameObject txtContPlayer2;
    private GameObject txtSensiPlayer2;
    private string[] controllers = new string[3];
    private List<string> conts = new List<string>();
    private int contValueControl;
    private int sensiValueControl;
    private float soundValueControl;
    private float musicValueControl;
    private float decSound;
    private float decMusic;
    private bool opMenu;
    private GameObject[] OptionsController;
    private GameObject OptionsMenu;
    private bool load = false;
    private bool activatedMenu = true;
    private bool init = true;
    private string controllerPlayer1 = "keyboard";
    private string controllerPlayer2 = "keyboard";
    private int sensiPlayer1 = 1;
    private int sensiPlayer2 = 1;
    private int joys;
    private bool start = false;
   

    void Awaken()
    {
        if (SceneManager.GetActiveScene().name == "Menu" && init == true)
        {
           
            mixer = Resources.Load<AudioMixer>("Effects");
            Cursor.visible = true;
            OptionsMenu = GameObject.Find("OptionsMenu");
            OptionsController = GameObject.FindGameObjectsWithTag("OptionsControllers");
            for (int x = 0; x < OptionsController.Length; x++)
            {
                if (OptionsController[x].name == "DropdownController")
                {
                    DropController = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "DropdownSensi")
                {
                    DropSensi = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "BtnClose")
                {
                    btnClose = OptionsController[x].GetComponent<Button>();
                }
                else if (OptionsController[x].name == "SldMusic")
                {
                    MusicSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "SlEffects")
                {
                    SoundSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "txtVolMusic")
                {
                    musicTxt = OptionsController[x].GetComponent<TextMeshProUGUI>();
                }
                else if (OptionsController[x].name == "DropdownControllerPlayer2")
                {
                    DropControllerPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    ControllerPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "DropdownSensiPlayer2")
                {
                    DropSensiPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    SensiPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtContPlayer2")
                {
                    txtContPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtSensiPlayer2")
                {
                    txtSensiPlayer2 = OptionsController[x];
                }
            }

            ControllerPlayer2.SetActive(false);
            SensiPlayer2.SetActive(false);
            txtContPlayer2.SetActive(false);
            txtSensiPlayer2.SetActive(false);
            btnClose.onClick.RemoveAllListeners();
            btnClose.onClick.AddListener(OnClicked);
            ToDec();
            mixer.SetFloat("musicVol", decMusic);
            mixer.SetFloat("soundVol", decSound);
            init = false;
        }
    }

    void Start()
    {
        Awaken();
        if (SceneManager.GetActiveScene().name == "Menu")
        {
            OptionsMenu.SetActive(false);
        }
        joys = GetNumJoys();
        UpdateCont(joys);

    }

    public void SendConfig()
    {
        
        if (SceneManager.GetActiveScene().name == "Juego" || SceneManager.GetActiveScene().name == "Multiplayer")
        {
            
            mixer = Resources.Load<AudioMixer>("Effects");
            OptionsMenu = GameObject.Find("OptionsMenu");
            OptionsController = GameObject.FindGameObjectsWithTag("OptionsControllers");
            for (int x = 0; x < OptionsController.Length; x++)
            {
                if (OptionsController[x].name == "DropdownController")
                {
                    DropController = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "DropdownSensi")
                {
                    DropSensi = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "BtnClose")
                {
                    btnClose = OptionsController[x].GetComponent<Button>();
                }
                else if (OptionsController[x].name == "SldMusic")
                {
                    MusicSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "SlEffects")
                {
                    SoundSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "txtVolMusic")
                {
                    musicTxt = OptionsController[x].GetComponent<TextMeshProUGUI>();
                }
                else if (OptionsController[x].name == "TxtVolSound")
                {
                    soundTxt = OptionsController[x].GetComponent<TextMeshProUGUI>();
                }
                else if (OptionsController[x].name == "DropdownControllerPlayer2")
                {
                    DropControllerPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    ControllerPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "DropdownSensiPlayer2")
                {
                    DropSensiPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    SensiPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtContPlayer2")
                {
                    txtContPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtSensiPlayer2")
                {
                    txtSensiPlayer2 = OptionsController[x];

                }
            }
            if (SceneManager.GetActiveScene().name == "Juego")
            {
                ControllerPlayer2.SetActive(false);
                SensiPlayer2.SetActive(false);
                txtContPlayer2.SetActive(false);
                txtSensiPlayer2.SetActive(false);
            }
            btnClose.onClick.AddListener(OnClicked);

            soundValueControl = soundVolume;
            musicValueControl = musicVolume;
            

        }
        if (SceneManager.GetActiveScene().name == "Menu")
        {
            mixer = Resources.Load<AudioMixer>("Effects");
            OptionsMenu = GameObject.Find("OptionsMenu");
            OptionsController = GameObject.FindGameObjectsWithTag("OptionsControllers");
            for (int x = 0; x < OptionsController.Length; x++)
            {
                if (OptionsController[x].name == "DropdownController")
                {
                    DropController = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "DropdownSensi")
                {
                    DropSensi = OptionsController[x].GetComponent<Dropdown>();
                }
                else if (OptionsController[x].name == "BtnClose")
                {
                    btnClose = OptionsController[x].GetComponent<Button>();
                }
                else if (OptionsController[x].name == "SldMusic")
                {
                    MusicSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "SlEffects")
                {
                    SoundSlider = OptionsController[x].GetComponent<Slider>();
                }
                else if (OptionsController[x].name == "txtVolMusic")
                {
                    musicTxt = OptionsController[x].GetComponent<TextMeshProUGUI>();
                }
                else if (OptionsController[x].name == "TxtVolSound")
                {
                    soundTxt = OptionsController[x].GetComponent<TextMeshProUGUI>();
                }
                else if (OptionsController[x].name == "DropdownControllerPlayer2")
                {
                    DropControllerPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    ControllerPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "DropdownSensiPlayer2")
                {
                    DropSensiPlayer2 = OptionsController[x].GetComponent<Dropdown>();
                    SensiPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtContPlayer2")
                {
                    txtContPlayer2 = OptionsController[x];
                }
                else if (OptionsController[x].name == "txtSensiPlayer2")
                {
                    txtSensiPlayer2 = OptionsController[x];
                }
            }
            
            btnClose.onClick.RemoveAllListeners();
            btnClose.onClick.AddListener(OnClicked);
            soundValueControl = soundVolume;
            musicValueControl = musicVolume;
            MusicSlider.value = musicVolume;
            SoundSlider.value = soundVolume;
            musicTxt.text = ToCentMusic() + " %";
            soundTxt.text = ToCentSound() + " %";
            
        }
    }

    void Update()
    {
        //if(SceneManager.GetActiveScene().name == "Menu" && activatedMenu == true)
        //{
        //    OptionsMenu.SetActive(false);
        //    activatedMenu = false;
        //}

        if(SceneManager.GetActiveScene().name == "Menu")
        {
            start = false;
        }

        if(SceneManager.GetActiveScene().name == "Menu" && load == true)
        {

            OptionsMenu.SetActive(true);
            load = false;
            OptionsMenu.SetActive(false);
            
        }
        if(SceneManager.GetActiveScene().name == "Juego" || SceneManager.GetActiveScene().name == "Menu" || SceneManager.GetActiveScene().name == "Multiplayer")
        {

           
            if (OptionsMenu.activeSelf == true)
            { 
               if(DropController.value != contValueControl)
                {
                    ChangeController1();
                    contValueControl = DropController.value;
                }
                if (DropSensi.value != sensiValueControl)
                {
                    ChangeSensibilidad(); 
                    sensiValueControl = DropSensi.value;
                }
                if(SoundSlider.value != soundValueControl)
                {
                    ChangeSoundVol();
                }
                if (MusicSlider.value != musicValueControl)
                {
                    ChangeMusicVol();
                }
            }
            if(start == false && (SceneManager.GetActiveScene().name == "Juego" || SceneManager.GetActiveScene().name == "Multiplayer"))
            {
                Debug.Log(SceneManager.GetActiveScene().name);
                start = true;
                joys = GetNumJoys();
                UpdateCont(joys);
            }
            
            if(joys != GetNumJoys())
            {
                joys = GetNumJoys();
                UpdateCont(joys);
            }
        }
    }

    public void Load()
    {
        load = true;
    }

    public void Resume()
    {
        GameManager.Instance.ResumeGame();
    }
    public void ToMenu()
    {
        GameManager.Instance.Gotomenu();
    }

    public void ChangeController1()
    {
        if (DropController.value == 0)
        {
            controllerPlayer1 = "keyboard";
        }
        else if(DropController.value == 1)
        {
            controllerPlayer1 = "joystic";
        }
        else
        {
            controllerPlayer1 = "joystic2";
        }
    }

    public void ChangeController2()
    {
        if (DropController.value == 0)
        {
            controllerPlayer1 = "keyboard";
        }
        else if (DropController.value == 1)
        {
            controllerPlayer1 = "joystic";
        }
        else
        {
            controllerPlayer1 = "joystic2";
        }
    }

    public void ChangeSensibilidad()
    {
        if (SceneManager.GetActiveScene().name == "Juego" || SceneManager.GetActiveScene().name == "Multiplayer")
        {
            Sensi = DropSensi.value;

            GameObject plyer;
            PlayerController plcont;

            plyer = GameObject.Find("Pala");
            plcont = plyer.GetComponent<PlayerController>();

            plcont.SetSensi(Sensi);

        }
        else
        {
            Sensi = DropSensi.value;
        }
    }

    public void CloseMenu()
    {
        if(SceneManager.GetActiveScene().name == "Menu")
        { 
            Cursor.visible = false;
            opMenu = false;
        }
        OptionsMenu.SetActive(false);
    }

    public void setOpMenu(bool opmenu)
    {
        opMenu = opmenu;
    }
    
    public bool getOpMenu()
    {
        return opMenu;
    }
    public void OpenMenu()
    {
        GameManager.Instance.OpenOptions();
        if (InputController.Instance.GetControler() == "joystick")
        {
            DropController.value = 1;
        }
        else
        {
            DropController.value = 0;
        }
        switch (Sensi)
        {
            case 0:
                DropSensi.value = 0;
                break;
            case 2:
                DropSensi.value = 2;
                break;
            default:
                DropSensi.value = 1;
                break;
        }
        MusicSlider.value = musicVolume;
        SoundSlider.value = soundVolume;
        musicTxt.text = ToCentMusic() + " %";
        soundTxt.text = ToCentSound() + " %";
    }
    public void OpenOpsMenu()
    {

        if (InputController.Instance.GetControler() == "joystick")
        {
            contValueControl = 1;
        }
        else
        {
            contValueControl = 0;
        }
        switch(Sensi)
        {
            case 0:
                DropSensi.value = 0;
                break;
            case 2:
                DropSensi.value = 2;
                break;
            default:
                DropSensi.value = 1;
                break;
        }

        
    }

    public void ChangeSoundVol()
    {
        soundVolume = SoundSlider.value;
        ToDec();
        mixer.SetFloat("soundVol", decSound);
        soundTxt.text = ToCentSound() + " %";
        soundValueControl = soundVolume;
    }
    public void ChangeMusicVol()
    {
        musicVolume = MusicSlider.value;
        ToDec();
        mixer.SetFloat("musicVol", decMusic);
        musicTxt.text = ToCentMusic() + " %";
        musicValueControl = musicVolume;
    }
    // Canvia valor decimal a decivelios
    private void ToDec()
    {
        float soundvol;
        float musicvol;

        soundvol = soundVolume / SoundSlider.maxValue;
        musicvol = musicVolume / MusicSlider.maxValue;

        decSound = Mathf.Log10(Mathf.Max(soundvol, 0.0001f)) * 20f;
        decMusic = Mathf.Log10(Mathf.Max(musicvol, 0.0001f)) * 20f;
    }

    // Canvia el valor de los sliders a por ciento
    private int ToCentMusic()
    {
        int percent;

        percent = (int)((musicVolume / MusicSlider.maxValue) * 100);

        return percent;

    }

    private int ToCentSound()
    {
        int percent;

        percent = (int)((soundVolume / SoundSlider.maxValue) * 100);

        return percent;

    }

    void OnClicked()
    {
        CloseMenu();
    }

    public void setControllerPlayer1(string Controller)
    {
        controllerPlayer1 = Controller;
    }
    public void setControllerPlayer2(string Controller)
    {
        controllerPlayer2 = Controller;
    }
    public string getControllerPlayer1()
    {
       return controllerPlayer1;
    }
    public string getControllerPlayer2()
    {
       return controllerPlayer2;
    }

    public void setSensiplayer2(string sensi)
    {
       switch (sensi)
        {
            case "Rapida":
                sensiPlayer2 = 3;
                break;
            case "Normal":
                sensiPlayer2 = 2;
                break;
            case "Lenta":
                sensiPlayer2 = 1;
                break;
        }
    }

    public void setSensiplayer1(string sensi)
    {
        switch (sensi)
        {
            case "Rapida":
                sensiPlayer1 = 3;
                break;
            case "Normal":
                sensiPlayer1 = 2;
                break;
            case "Lenta":
                sensiPlayer1 = 1;
                break;
        }
    }

    public int getSensiPlayer1()
    {
        return sensiPlayer1;
    }

    public int getSensiPlayer2()
    {
        return sensiPlayer2;
    }

    public string[] getControllerList(int nJoys)
    {
        string[] cArray = new string[3];

        cArray[0] = "keyboard";

        if(nJoys > 0)
        { 
            for (int x = 0; x < cArray.Length ; x++)
            {
                if(Input.GetJoystickNames().Length >= 2)
                {
                    Debug.Log($"Entra AQUI!!! X: {x}");
                    if (x == 1)
                    {
                        cArray[x] = "joystic";
                    }
                    else if (x != 0 && x != 1)
                    {
                        cArray[x] = $"joystic{x - 1}";
                    }
                }
                else
                {
                    cArray[1] = "joystic";
                }
            }
        }

        
        return cArray;
    }

    public void UpdateCont(int nmJoys)
    {
        DropController.ClearOptions();
        DropControllerPlayer2.ClearOptions();
        conts.Clear();
        controllers = getControllerList(nmJoys);
        for (int x = 0; x < controllers.Length; x++)
        {
            if(controllers[x] != null)
            {
                conts.Add(controllers[x]);
            }
   
        }

        DropController.AddOptions(conts);
        DropControllerPlayer2.AddOptions(conts);
    }

    public int GetNumJoys()
    {
        int numJoys = 0;
        for(int x = 0; x < Input.GetJoystickNames().Length; x++)
        {
            if(Input.GetJoystickNames()[x] != null && Input.GetJoystickNames()[x] != "" && Input.GetJoystickNames()[x] != " ")
            {
                numJoys++;
            }

        }
        return numJoys;
    }
}
