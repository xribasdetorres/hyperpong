﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UIController : MonoBehaviour
{
    public TextMeshProUGUI TiempoPower1;
    public TextMeshProUGUI TiempoPower2;
    public Image ImagePower1;
    public Image ImagePower2;
    public TextMeshProUGUI TiempoPowerEnemy1;
    public TextMeshProUGUI TiempoPowerEnemy2;
    public Image ImagePowerEnemy1;
    public Image ImagePowerEnemy2;
    public Sprite Power1ImageAct;
    public Sprite Power1ImageNoDisponible;
    public Sprite Power1ImageDisp;
    public Sprite Power2ImageAct;
    public Sprite Power2ImageNoDisponible;
    public Sprite Power2ImageDisp;
    public bool showUIEnemy;


    private estadosPowers estadoPower1;
    private estadosPowers estadoPower2;
    private float timePower1;
    private float timePower2;
    private estadosPowers estadoPowerEn1;
    private estadosPowers estadoPowerEn2;
    private float timePowerEn1;
    private float timePowerEn2;
    private bool activatePower1;
    private bool activatePower2;
    private GameObject PalaPlayer;
    private GameObject PalaEnemy;
    private PlayerController PlController;
    private Player2Controller Pl2Controller;
    private IAEnemy IAcontroller;

    // Start is called before the first frame update
    void Start()
    {
        PalaPlayer = GameObject.FindGameObjectWithTag("Player1");
        if(GameObject.FindGameObjectWithTag("Enemy") != null)
        {
            PalaEnemy = GameObject.FindGameObjectWithTag("Enemy");
            IAcontroller = PalaEnemy.GetComponent<IAEnemy>();
            timePowerEn1 = IAcontroller.getTime();
            timePowerEn2 = IAcontroller.getTime2();
        }
        else
        {
            PalaEnemy = GameObject.FindGameObjectWithTag("Player2");
            Pl2Controller = PalaEnemy.GetComponent<Player2Controller>();
            timePowerEn1 = Pl2Controller.getTime();
            timePowerEn2 = Pl2Controller.getTime2();
        }
        PlController = PalaPlayer.GetComponent<PlayerController>();
        IAcontroller = PalaEnemy.GetComponent<IAEnemy>();
        timePower1 = PlController.getTime();
        timePower2 = PlController.getTime2();
        activatePower1 = PlController.getActivated();
        
        TiempoPower1.text = (Mathf.Round(timePower1)).ToString();
        TiempoPower2.text = (Mathf.Round(timePower2)).ToString();
        ImagePower1.sprite = Power1ImageNoDisponible;
        ImagePowerEnemy1.sprite = Power1ImageNoDisponible;
        ImagePower2.sprite = Power2ImageNoDisponible;
        ImagePowerEnemy2.sprite = Power2ImageNoDisponible;
    }

    // Update is called once per frame
    void Update()
    {

        if (GameObject.FindGameObjectWithTag("Enemy") != null)
        {
            timePowerEn1 = IAcontroller.getTime();
            timePowerEn2 = IAcontroller.getTime2();
            estadoPowerEn1 = IAcontroller.getEstadoP1();
        }
        else
        {
            timePowerEn1 = Pl2Controller.getTime();
            timePowerEn2 = Pl2Controller.getTime2();
            estadoPowerEn1 = Pl2Controller.getEstadoPower1();
            estadoPowerEn2 = Pl2Controller.getEstadoPower2();
        }


        timePower1 = PlController.getTime();
        estadoPower1 = PlController.getEstadoPower1();
        timePower2 = PlController.getTime2();
        estadoPower2 = PlController.getEstadoPower2();


        TiempoPower1.text = (Mathf.Round(timePower1)).ToString();
        TiempoPower2.text = (Mathf.Round(timePower2)).ToString();
        if (estadoPower1 == estadosPowers.Activado)
        {
            ImagePower1.sprite = Power1ImageAct;
        }
        else if(estadoPower1 == estadosPowers.Disponibe)
        {
            ImagePower1.sprite = Power1ImageDisp;
        }
        else
        {
            ImagePower1.sprite = Power1ImageNoDisponible;
        }
        if (estadoPower2 == estadosPowers.Activado)
        {
            ImagePower2.sprite = Power2ImageAct;
        }
        else if (estadoPower2 == estadosPowers.Disponibe)
        {
            ImagePower2.sprite = Power2ImageDisp;
        }
        else
        {
            ImagePower2.sprite = Power2ImageNoDisponible;
        }

        if (showUIEnemy == true)
        {
            ImagePowerEnemy1.enabled = true;
            TiempoPowerEnemy1.enabled = true;
            ImagePowerEnemy2.enabled = true;
            TiempoPowerEnemy2.enabled = true;

            TiempoPowerEnemy1.text = (Mathf.Round(timePowerEn1)).ToString();
            TiempoPowerEnemy2.text = (Mathf.Round(timePowerEn2)).ToString();
            if (estadoPowerEn1 == estadosPowers.Activado)
            {
                ImagePowerEnemy1.sprite = Power1ImageAct;
            }
            else if (estadoPowerEn1 == estadosPowers.Disponibe)
            {
                ImagePowerEnemy1.sprite = Power1ImageDisp;
            }
            else
            {
                ImagePowerEnemy1.sprite = Power1ImageNoDisponible;
            }
            if (estadoPowerEn2 == estadosPowers.Activado)
            {
                ImagePowerEnemy2.sprite = Power2ImageAct;
            }
            else if (estadoPowerEn2 == estadosPowers.Disponibe)
            {
                ImagePowerEnemy2.sprite = Power2ImageDisp;
            }
            else
            {
                ImagePowerEnemy2.sprite = Power2ImageNoDisponible;
            }
        }
        else
        {
            ImagePowerEnemy1.enabled = false;
            TiempoPowerEnemy1.enabled = false;
        }
    }
    
}
