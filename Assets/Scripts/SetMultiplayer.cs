﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SetMultiplayer : MonoBehaviour
{


    [SerializeField]
    private ControllerSelector[] ContSelector;
    [SerializeField]
    private SensiScript[] SensibilidadScript;
    [SerializeField]
    private Transform Selector;
    [SerializeField]
    private Transform[] Positions;
    [SerializeField]
    private MaxPointsSelector MaxPS;
    [SerializeField]
    private GameObject[] SensibilidadObject;

    private string controllerPlayer1;
    private string controllerPlayer2;
    private int points;
    private bool Selected;
    private string controller;
    private bool JD = false;
    private bool JU = false;


    void Start()
    {
        controller = InputController.Instance.GetControler();
        Selected = false;
        points = 0;

    }
    void Update()
    {
        controllerPlayer1 = ContSelector[0].getController2();
        controllerPlayer2 = ContSelector[1].getController2();
        controller = InputController.Instance.GetControler();
        if (controller == "joystick")
        {

            if (InputController.Instance.getSubmitButton())
            {
                Submit();
            }
            if (InputController.Instance.getAxisYJoystic() > 0 && JU == false && Selected == false)
            {
                JU = true;
                GoUp();
            }
            if (InputController.Instance.getAxisYJoystic() < 0 && JD == false && Selected == false)
            {
                JD = true;
                GoDown();
            }
            if (InputController.Instance.getAxisYJoystic() == 0)
            {
                JD = false;
                JU = false;
            }
        }

        if (controller == "keyboard")
        {
            if (InputController.Instance.getSubmitKeyButton())
            {
                Submit();
            }
            if (InputController.Instance.getKeyUp() && Selected == false)
            {
                GoUp();
            }
            if (InputController.Instance.getKeyDown() && Selected == false)
            {
                GoDown();
            }
        }
        if(controllerPlayer1 == "joystic")
        {
            SensibilidadObject[0].SetActive(false);
        }
        else
        {
            SensibilidadObject[0].SetActive(true);
        }
        if (controllerPlayer2 == "joystic")
        {
            SensibilidadObject[1].SetActive(false);
        }
        else
        {
            SensibilidadObject[1].SetActive(true);
        }
    }


    private void GoUp()
    {
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[0].position;
        }
        if (Selector.position == Positions[2].position)
        {
            Selector.position = Positions[1].position;
        }
        if (Selector.position == Positions[3].position)
        {
            Selector.position = Positions[2].position;
        }
        if (Selector.position == Positions[4].position)
        {
            Selector.position = Positions[3].position;
        }
        if (Selector.position == Positions[5].position)
        {
            Selector.position = Positions[4].position;
        }
    }

    private void GoDown()
    {
        if (Selector.position == Positions[4].position)
        {
            Selector.position = Positions[5].position;
        }
        if (Selector.position == Positions[3].position)
        {
            Selector.position = Positions[4].position;
        }
        if (Selector.position == Positions[2].position)
        {
            Selector.position = Positions[3].position;
        }
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[2].position;
        }
        if (Selector.position == Positions[0].position)
        {
            Selector.position = Positions[1].position;
        }
    }

    private void Submit()
    {
        RaycastHit hitinfo;
        if (Physics.Raycast(Selector.position, new Vector3(1, 0, 0), out hitinfo, 1000) == true)
        {
            if (hitinfo.transform.name == "Player1Controller")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                ContSelector[0].SetSelected();
            }
            else if (hitinfo.transform.name == "SensibilidadPlayer1")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                SensibilidadScript[0].SetSelected();
            }
            else if(hitinfo.transform.name == "Player2Controller")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                ContSelector[1].SetSelected();
            }
            else if (hitinfo.transform.name == "SensibilidadPlayer2")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                SensibilidadScript[1].SetSelected();
            }
            else if (hitinfo.transform.name == "MaxPuntB")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                MaxPS.SetSelected();
            }
            else
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                setGame();
            }
        }
    }

    private void setGame()
    {
        Config.Instance.setControllerPlayer1(ContSelector[0].getController2());
        Config.Instance.setControllerPlayer2(ContSelector[1].getController2());
        if(ContSelector[0].getController2() == "keyboard")
        {
            Config.Instance.setSensiplayer1(SensibilidadScript[0].GetSensibilidad());
        }
        if (ContSelector[1].getController2() == "keyboard")
        {
            Config.Instance.setSensiplayer2(SensibilidadScript[1].GetSensibilidad());
        }
        GameManager.Instance.setMGame(MaxPS.getMaxPoints());
        SceneManager.LoadScene("_CargaM");
    }


}
