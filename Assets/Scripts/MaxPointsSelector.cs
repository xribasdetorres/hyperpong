﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MaxPointsSelector : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Arrows;
    [SerializeField]
    private TextMeshProUGUI Pointstxt;

    private bool Selected;
    private int Points;
    private bool PressJL;
    private bool PressJR;

    // Start is called before the first frame update
    void Start()
    {
        Points = 0;
        Pointstxt.text = Points.ToString();
        PressJL = false;
        PressJR = false;
    }

    // Update is called once per frame
    void Update()
    {
        Pointstxt.text = Points.ToString();
        if (Selected)
        {
            if(Points > 0)
            {
                if(InputController.Instance.getAxisXJoystic() < 0)
                {
                    if(PressJR == false)
                    {
                        PressJR = true;
                        --Points;
                    }
                    
                }
                if ( InputController.Instance.getKeyLeft())
                {
                    --Points;
                }
            }
            if (InputController.Instance.getAxisXJoystic() > 0 || InputController.Instance.getKeyRight())
            {
                if(PressJL == false)
                {
                    PressJL = true;
                    ++Points;
                }
               
            }
            if (InputController.Instance.getKeyRight())
            {
                ++Points;
            }
        }
        if(InputController.Instance.getAxisXJoystic() == 0)
        {
            PressJL = false;
            PressJR = false;
        }
        if (Points == 0)
        {
            Arrows[0].SetActive(false);
        }
        if (Points > 0)
        {
            Arrows[0].SetActive(true);
        }
    }
    public void SetSelected()
    {
        if (Selected == false)
        {
            Selected = true;
        }
        else
        {
            Selected = false;
        }
    }
    public int getMaxPoints()
    {
        int mPoints = 0;
        mPoints = Points;
        return mPoints;
    }
}
