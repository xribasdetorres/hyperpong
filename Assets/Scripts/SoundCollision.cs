﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCollision : MonoBehaviour
{
    private AudioSource bump;

    // Start is called before the first frame update
    void Start()
    {
        bump = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.collider.tag == "Pelota")
        {
            bump.Play();
        }
    }
   
}
