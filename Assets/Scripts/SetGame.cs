﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public enum DificSelection
{
    Facil,
    Normal,
    Dificil
}

public class SetGame : MonoBehaviour
{
    

    

    [SerializeField]
    private DificultySelector DifSelector;
    [SerializeField]
    private Transform Selector;
    [SerializeField]
    private Transform[] Positions;
    [SerializeField]
    private MaxPointsSelector MaxPS;

    private DificSelection dificultad;
    private int points;
    private bool Selected;
    private string controller;
    private bool JD = false;
    private bool JU = false;


    void Start()
    {
        controller = InputController.Instance.GetControler();
        Selected = false;
        points = 0;
        
    }
    void Update()
    {
        controller = InputController.Instance.GetControler();
        if (controller == "joystick")
        {
            
            if (InputController.Instance.getSubmitButton())
            {
                Submit();
            }
            if (InputController.Instance.getAxisYJoystic() > 0 && JU == false && Selected == false)
            {
                JU = true;
                GoUp();
            }
            if (InputController.Instance.getAxisYJoystic() < 0 && JD == false && Selected == false)
            {
                JD = true;
                GoDown();
            }
            if(InputController.Instance.getAxisYJoystic() == 0)
            {
                JD = false;
                JU = false;
            }
        }

        if (controller == "keyboard")
        {
            if (InputController.Instance.getSubmitKeyButton())
            {
                Submit();
            }
            if (InputController.Instance.getKeyUp() && Selected == false)
            {
                GoUp();
            }
            if (InputController.Instance.getKeyDown() && Selected == false)
            {
                GoDown();
            }
        }
    }


    private void GoUp()
    {
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[0].position;
        }
        if (Selector.position == Positions[2].position)
        {
            Selector.position = Positions[1].position;
        }
    }

    private void GoDown()
    {
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[2].position;
        }
        if (Selector.position == Positions[0].position)
        {

            Selector.position = Positions[1].position;
        }
    }

    private void Submit()
    {
        RaycastHit hitinfo;
        if (Physics.Raycast(Selector.position, new Vector3(1, 0, 0), out hitinfo, 1000) == true)
        {
            if (hitinfo.transform.name == "Dificulty")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                DifSelector.SetSelected();
            }
            else if (hitinfo.transform.name == "MaxPuntB")
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                MaxPS.SetSelected();
            }
            else
            {
                if (Selected)
                {
                    Selected = false;
                }
                else
                {
                    Selected = true;
                }
                setGame();
            }
        }
    }

    private void setGame()
    {
        
        GameManager.Instance.setGame(DifSelector.getDificultat(), MaxPS.getMaxPoints());
        SceneManager.LoadScene("_Carga");
    }

}
