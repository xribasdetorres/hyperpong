﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class DificultySelector : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Arrows;
    [SerializeField]
    private TextMeshProUGUI DifText;

    private DificSelection SelectedDif;
    private bool Selected;
    private bool pressJL;
    private bool pressJR;

    // Start is called before the first frame update
    void Start()
    {
        pressJL = false;
        pressJR = false;
        SelectedDif = DificSelection.Facil;
    }

    // Update is called once per frame
    void Update()
    {
        if(Selected)
        {
            if(InputController.Instance.getAxisXJoystic() > 0)
            {
                if (pressJR == false)
                {
                    pressJR = true;
                    switch (SelectedDif)
                    {
                        case DificSelection.Facil:
                            SelectedDif = DificSelection.Normal;
                            break;
                        case DificSelection.Normal:
                            SelectedDif = DificSelection.Dificil;
                            break;
                    }
                }
            }
            if (InputController.Instance.getKeyRight())
            {
                switch (SelectedDif)
                {
                    case DificSelection.Facil:
                        SelectedDif = DificSelection.Normal;
                        break;
                    case DificSelection.Normal:
                        SelectedDif = DificSelection.Dificil;
                        break;
                }

            }
            if (InputController.Instance.getAxisXJoystic() < 0)
            {
                if (pressJL == false)
                {
                    pressJL = true;
                    switch (SelectedDif)
                    {
                        case DificSelection.Dificil:
                            SelectedDif = DificSelection.Normal;
                            break;
                        case DificSelection.Normal:
                            SelectedDif = DificSelection.Facil;
                            break;
                    }
                }
            }
            if ( InputController.Instance.getKeyLeft())
            {
                switch (SelectedDif)
                {
                    case DificSelection.Dificil:
                        SelectedDif = DificSelection.Normal;
                        break;
                    case DificSelection.Normal:
                        SelectedDif = DificSelection.Facil;
                        break;
                }

            }
            if (InputController.Instance.getAxisXJoystic() == 0)
            {
                pressJL = false;
                pressJR = false;
            }
        }
        switch(SelectedDif)
        {
            case DificSelection.Facil:
                DifText.text = "Facil";
                Arrows[0].SetActive(false);
                Arrows[1].SetActive(true);
                break;
            case DificSelection.Normal:
                DifText.text = "Normal";
                Arrows[0].SetActive(true);
                Arrows[1].SetActive(true);
                break;
            case DificSelection.Dificil:
                DifText.text = "Dificil";
                Arrows[0].SetActive(true);
                Arrows[1].SetActive(false);
                break;

        }
    }
    public void SetSelected()
    {
        if(Selected == false)
        {
            Selected = true;
        }
        else
        {
            Selected = false;
        }
    }

    public DificSelection getDificultat()
    {
        return SelectedDif;
    }

}
