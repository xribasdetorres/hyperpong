﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TipoPowerUps
{
    //en teoria hay 3 power ups
    Rebote,
    MasPotencia,
    Tipo3
}
public class CajaPowerUp : MonoBehaviour
{

    [SerializeField]
    public TipoPowerUps TipoPowerUp;


    [SerializeField]
    private Material[] MaterialsPower;


    private MeshRenderer meshRander;


    // Start is called before the first frame update
    void Start()
    {
        meshRander = GetComponent<MeshRenderer>();
        if(TipoPowerUp == TipoPowerUps.Rebote)
        {
            meshRander.material = MaterialsPower[1];
        }
        else if(TipoPowerUp == TipoPowerUps.MasPotencia)
        {
            meshRander.material = MaterialsPower[2];
        }
        else
        {
            meshRander.material = MaterialsPower[0];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetTipoPowerUp(TipoPowerUps tipPow)
    {
        TipoPowerUp = tipPow;
    }
    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Meta" || col.tag == "Player1")
        {
            GameManager.Instance.getPU();
            Destroy(gameObject);
            Debug.Log("Destruido PU");
        }

    }
}
