﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaScript : MonoBehaviour
{
    private Rigidbody thisrigid;
    [SerializeField]
    private float maxY;
    [SerializeField]
    private float minY;
    private bool PelAfuera;
    [SerializeField]
    private Gradient PlayerGradient;
    [SerializeField]
    private Gradient EnemyGradient;    

    private GameObject Enemy;
    private IAEnemy IA;
    private LineRenderer trace;
    private TrailRenderer trail;
    private Vector3 startPos;
    private int points = 1;

    // Start is called before the first frame update
    void Start()
    {
        thisrigid = GetComponent<Rigidbody>();
        PelAfuera = false;
        Enemy = GameObject.Find("PlayerEnemy");
        if(Enemy != null)
        {
            IA = Enemy.GetComponent<IAEnemy>();
        }
        trail = GetComponent<TrailRenderer>();
        //trace = GetComponent<LineRenderer>();
        //trace.SetPosition(0, transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(transform.position.y > maxY || transform.position.y < minY)
        {

            Destroy(gameObject);
        }
        
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.collider.tag == "Pared" && (col.collider.tag != "Player1" && col.collider.tag != "Enemy" && col.collider.tag != "Meta"))
        {
            
            if (Mathf.Round(thisrigid.velocity.x) == 0 || (transform.position.y > maxY || transform.position.y < minY))
            {
                Destroy(gameObject);
            }
            


        }
        //if(col.collider.tag == "Pared" || col.collider.tag == "Player1" || col.collider.tag == "Enemy" || col.collider.tag == "Meta")
        //{

        //    trace.SetPosition(points, transform.position);
        //    ++points;
        //}
        if (col.collider.tag == "Enemy")
        {
            trail.colorGradient = EnemyGradient;
            //trail.material.color = Color.red;
        }
        if (col.collider.tag == "Player1")
        {
            trail.colorGradient = PlayerGradient;
            //trail.material.color = Color.green;
        }
    }

}
