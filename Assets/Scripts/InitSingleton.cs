﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InitSingleton : MonoBehaviour
{

    [SerializeField]
    private AudioSource Musica;
    private GameObject menu;

    private bool sendedConfi = false;

    // Start is called before the first frame update
    void Awake()
    {
        if(GameObject.Find("Manager") == null)
        {
            Debug.Log("No encontrado el GameManager");
            return;
        }

        Config.Instance.SendConfig();
        



        
    }

    void Start()
    {
        if(SceneManager.GetActiveScene().name == "Juego")
        {
            GameManager.Instance.StartGame();
        }
        if(SceneManager.GetActiveScene().name == "Multiplayer")
        {
            GameManager.Instance.StartGameM();
        }
    }


    public void OpenOptionsMenu()
    {
        Config.Instance.OpenMenu();
    }

    public void ResumeGame()
    {
        Config.Instance.Resume();
    }
    
    public void Menu()
    {
        Config.Instance.Load();
        Config.Instance.ToMenu();

    }
    public void StartMusic()
    {
        Musica.Play();
    }
}
