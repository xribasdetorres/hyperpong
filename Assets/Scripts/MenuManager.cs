﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private Transform Selector;
    [SerializeField]
    private GameObject SelObj;
    [SerializeField]
    private Transform[] Positions;
    [SerializeField]
    private GameObject OptionsMenu;
    [SerializeField]
    private GameObject ConfigEL;
    private string[] Controller = new string[3];
    private string[] ControllerConf = new string[3];

    private Config cfg;
    private string controller;
    private bool opsMenu;
    private bool JD = false;
    private bool JU = false;
    private bool initiated = false;

    // Start is called before the first frame update

    void Awake()
    {

        cfg = ConfigEL.GetComponent<Config>();
    }

    void Start()
    {
        
        SceneManager.sceneLoaded += OnSceneLoad;
        controller = InputController.Instance.GetControler();
        opsMenu = false;
        Cursor.visible = false;

    }

    void OnSceneLoad(Scene scene, LoadSceneMode mode)
    {
        Config.Instance.SendConfig();
    }

    // Update is called once per frame
    void Update()
    {
        if (ConfigEL == null)
        {
            opsMenu = Config.Instance.getOpMenu();
        }
        else
        {
            opsMenu = cfg.getOpMenu();
        }
        
        controller = InputController.Instance.GetControler();
        if (controller == "joystick" && opsMenu == false)
        {
            if (InputController.Instance.getSubmitButton())
            {
                Submit();
            }
            if (InputController.Instance.getAxisYJoystic() > 0 && JU == false)
            {
                JU = true;
                GoUp();
            }
            if (InputController.Instance.getAxisYJoystic() < 0 && JD == false)
            {
                JD = true;
                GoDown();
            }
            if(InputController.Instance.getAxisYJoystic() == 0)
            {
                JD = false;
                JU = false;
            }
        }

        if(controller == "keyboard" && opsMenu == false)
        {
            
            if (InputController.Instance.getSubmitKeyButton())
            {
                Submit();
            }
            if (InputController.Instance.getKeyUp())
            {
                GoUp();
            }
            if (InputController.Instance.getKeyDown())
            {
                GoDown();
            }
        }
        if(Input.GetKeyDown(KeyCode.M) == true)
        {
            SceneManager.LoadScene("_CargaM");
        }
        
        
        
    }

    public void LoadGame()
    {

        SceneManager.LoadScene("SetGame");
    }
    public void ExitGame()
    {
        Application.Quit();
    }

    private void GoUp()
    {
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[0].position;
        }
        if (Selector.position == Positions[2].position)
        {
            Selector.position = Positions[1].position;
        }
        if (Selector.position == Positions[3].position)
        {
            Selector.position = Positions[2].position;
        }
    }

    private void GoDown()
    {
        if (Selector.position == Positions[2].position)
        {
            Selector.position = Positions[3].position;
        }
        if (Selector.position == Positions[1].position)
        {
            Selector.position = Positions[2].position;
        }
        if (Selector.position == Positions[0].position)
        {
            Selector.position = Positions[1].position;
        }
    }
    private void Submit()
    {
        RaycastHit hitinfo;
        if (Physics.Raycast(Selector.position, Selector.TransformDirection(Vector3.left), out hitinfo) == true)
        {
            if (hitinfo.transform.name == "Start")
            {
                initiated = false;
                LoadGame();
                
            }
            else if (hitinfo.transform.name == "Options")
            {
                OptionsMenu.SetActive(true);
                cfg.OpenOpsMenu();
                Cursor.visible = true;
                opsMenu = true;
                cfg.setOpMenu(opsMenu);
            }
            else if(hitinfo.transform.name == "Multiplayer")
            {
                SceneManager.LoadScene("SetMultiplayer");
            }
            else
            {
                ExitGame();
            }

        }
    }
}
