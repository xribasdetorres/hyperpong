﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SensiScript : MonoBehaviour
{
    public TextMeshProUGUI textSensi;
    private string[] sensibilidad = { "Rapida", "Normal", "Lenta" };
    private string sensiActual;
    private int indexActSensi;

    [SerializeField]
    private GameObject[] arrows;

    private bool Selected;
    private bool pressJL;
    private bool pressJR;

    // Start is called before the first frame update
    void Start()
    {
        pressJL = false;
        pressJR = false;
        indexActSensi = 1;
        sensiActual = sensibilidad[indexActSensi];
        textSensi.text = sensiActual; 
    }

    // Update is called once per frame
    void Update()
    {
        if (Selected)
        {
            if (InputController.Instance.getAxisXJoystic() > 0)
            {
                if (pressJR == false)
                {
                    pressJR = true;
                    if(sensiActual != sensibilidad[2])
                    {
                        indexActSensi++;
                        sensiActual = sensibilidad[indexActSensi];
                        textSensi.text = sensiActual;
                    }
                }
            }
            if (InputController.Instance.getKeyRight())
            {
                if (sensiActual != sensibilidad[2])
                {
                    indexActSensi++;
                    sensiActual = sensibilidad[indexActSensi];
                    textSensi.text = sensiActual;
                }
            }
            if (InputController.Instance.getAxisXJoystic() < 0)
            {
                if (pressJL == false)
                {
                    pressJL = true;
                    if (sensiActual != sensibilidad[0])
                    {
                        indexActSensi--;
                        sensiActual = sensibilidad[indexActSensi];
                        textSensi.text = sensiActual;
                    }
                }
            }
            if (InputController.Instance.getKeyLeft())
            {
                if (sensiActual != sensibilidad[0])
                {
                    indexActSensi--;
                    sensiActual = sensibilidad[indexActSensi];
                    textSensi.text = sensiActual;
                }
            }
            if (InputController.Instance.getAxisXJoystic() == 0)
            {
                pressJL = false;
                pressJR = false;
            }
            if (sensiActual == sensibilidad[0])
            {
                arrows[0].SetActive(false);
            }
            {
                arrows[1].SetActive(false);
            }
            if (sensiActual != sensibilidad[0])
            {
                arrows[0].SetActive(true);
            }
            if (sensiActual != sensibilidad[2])
            {
                arrows[1].SetActive(true);
            }
        }
    }

    public void SetSelected()
    {
        if (Selected == false)
        {
            Selected = true;
        }
        else
        {
            Selected = false;
        }
    }

    public string GetSensibilidad()
    {
        return sensiActual;
    }
}
