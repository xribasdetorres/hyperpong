﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gol : MonoBehaviour
{
    private GameObject Pelota;
    public GameObject Pala;

    [SerializeField]
    private float Potencia = 0;
    private PlayerController plControler;
    private Player2Controller pl2Controler;
    private IAEnemy Ia;
    private Player2Controller pl2Controller;
    private bool activateCollision;
    private Collider colider;
    private Rigidbody rgbdPelota;
    //private bool doblepotencia;
    [SerializeField]
    Material ColPel;
    [SerializeField]
    Material Normal;

    // Start is called before the first frame update
    void Start()
    {
        if(Pala.tag == "Player1")
        {
            plControler = Pala.GetComponent<PlayerController>();
            activateCollision = plControler.getActivated();
        }
        if (Pala.tag == "Player2")
        {
            pl2Controler = Pala.GetComponent<Player2Controller>();
            activateCollision = pl2Controler.getActivated();
        }
        if (Pala.tag == "Enemy")
        {
            if(Pala.GetComponent<IAEnemy>() != null)
            {
                Ia = Pala.GetComponent<IAEnemy>();
                activateCollision = Ia.getActivated();
            }
            else
            {
                pl2Controller = Pala.GetComponent<Player2Controller>();
                activateCollision = pl2Controller.getActivated();
            }            
        }
        colider = GetComponent<Collider>();
        Pelota = GameObject.FindGameObjectWithTag("Pelota");
        rgbdPelota = Pelota.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Pala.tag == "Player1")
        {
            activateCollision = plControler.getActivated();
            if(activateCollision)
            {
                gameObject.GetComponent<MeshRenderer>().material = ColPel;
                colider.isTrigger = false;
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().material = Normal;
                colider.isTrigger = true;
            }
        }
        if (Pala.tag == "Enemy")
        {
            activateCollision = Ia.getActivated();
            if (activateCollision)
            {
                gameObject.GetComponent<MeshRenderer>().material = ColPel;
                colider.isTrigger = false;
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().material = Normal;
                colider.isTrigger = true;
            }
        }
        if (Pala.tag == "Player2")
        {
            activateCollision = pl2Controler.getActivated();
            if (activateCollision)
            {
                gameObject.GetComponent<MeshRenderer>().material = ColPel;
                colider.isTrigger = false;
            }
            else
            {
                gameObject.GetComponent<MeshRenderer>().material = Normal;
                colider.isTrigger = true;
            }
        }
        if (GameObject.Find("Manager") == null)
        {
            Debug.Log("No encontrado el GameManager");
        }
        if (Pelota == null)
        {
            Pelota = GameObject.FindGameObjectWithTag("Pelota");
            rgbdPelota = Pelota.GetComponent<Rigidbody>();
        }
    }
    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Pelota")
        {
            if(this.gameObject.name == "Izquierda")
            {
                GameManager.Instance.AddPuntacionJug2();
            }
            else
            {
                GameManager.Instance.AddPuntacionJug1();
            }
            Destroy(Pelota);
            
        }
    }
    void OnCollisionEnter(Collision col)
    {

        if (col.collider.tag == "Pelota")
        {

            foreach (ContactPoint contact in col.contacts)
            {

                rgbdPelota.AddForce(-1 * contact.normal.x * Potencia, -1 * contact.normal.y * Potencia, 0, ForceMode.Impulse);

            }


        }

    }
}

