﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Singleton<InputController>
{

    private string ControllerString = "Keyboard";

    public string GetControler()
    {
        string Controller;
        if(ControllerString == "Joystic" && Input.GetJoystickNames().Length > 0)
        {
            Controller = "joystic";
        }
        else
        {
            Controller = "keyboard";
        }
        return Controller;
    }
    public float getAxisYJoystic()
    {
        return Input.GetAxis("Vertical2");
    }
    public float getAxisYJoystic1()
    {
        return Input.GetAxis("Vertical3");
    }

    public int JoysticsNumb()
    {
        return Input.GetJoystickNames().Length;
    }
    public float getAxisYKeyboard()
    {
        return Input.GetAxis("Vertical");
    }
    public float getAxisXJoystic()
    {
        return Input.GetAxis("Horizontal2");
    }
    public float getAxisXKeyboard()
    {
        return Input.GetAxis("Horizontal");
    }
    public bool getKeyDown()
    {
        return Input.GetKeyDown(KeyCode.DownArrow);
    }
    public bool getKeyUp()
    {
        return Input.GetKeyDown(KeyCode.UpArrow);
    }
    public bool getKeyLeft()
    {
        return Input.GetKeyDown(KeyCode.LeftArrow);
    }
    public bool getKeyRight()
    {
        return Input.GetKeyDown(KeyCode.RightArrow);
    }
    public bool getEscButton()
    {

        return Input.GetKeyDown(KeyCode.Escape);
    }

    public bool getStartButton()
    {
        return Input.GetButtonDown("StartJ1");
    }

    public bool getSubmitButton()
    {
        return Input.GetButtonDown("SubmitJoy");
        
    }

    public bool getSubmitKeyButton()
    {
        return Input.GetButtonDown("SubmitKey");
        
    }
    
    public bool GetPower1KeyButton()
    {
        return Input.GetKeyDown(KeyCode.Alpha1);
    }
    public bool GetPower2KeyButton()
    {
        return Input.GetKeyDown(KeyCode.Alpha2);
    }

    public bool GetPower1JoyButton()
    {
        return Input.GetButtonDown("Fire1");
    }

    public bool GetPower2JoyButton()
    {
        return Input.GetButtonDown("Fire2");
    }
    public bool GetPower1Joy1Button()
    {
        return Input.GetButtonDown("Fire12");
    }

    public bool GetPower2Joy1Button()
    {
        return Input.GetButtonDown("Fire22");
    }

    public bool GetCreatePowerUP()
    {
        return Input.GetKeyDown(KeyCode.P);
    }

    public void SetControllerString(string CString)
    {
        ControllerString = CString;
    }
}
