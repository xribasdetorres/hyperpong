﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitScript : MonoBehaviour
{
    [SerializeField]
    private GameObject ConfigEL;

    // Start is called before the first frame update
    void Start()
    {
        if (ConfigEL == null)
        {
                Config.Instance.SendConfig();
        }
    }
}
