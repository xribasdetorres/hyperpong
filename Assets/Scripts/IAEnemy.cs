﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IAEnemy : MonoBehaviour
{
    
    public DificSelection IAdificultat;
    public int Potencia;
    [Header("Velocidad dependiendo de la dificultat")]
    [SerializeField] private float[] difVel;
    [Range(0, 60)]
    public float timePowers;

    private float veloc;
    private Transform pala;
    private Transform pelotatrans;
    private Rigidbody rgbd;
    private Rigidbody rgbdPelota;
    private PelotaScript pelScript;
    private GameObject Pelota;
    private estadosPowers estadoPower1;
    private estadosPowers estadoPower2;
    private int diferenciaPuntos;
    private AudioSource bumppala;
    [SerializeField]
    private int[] maxDifeDif = new int[3];
    [SerializeField]
    private int[] maxDisDif = new int[3];
    private float currentTimePower;
    private float currentTimePower2;

    private int maxDif;
    private int maxDis;
    private bool Power1Act;
    private bool Power2Act;
    private bool Afuera;


    // Start is called before the first frame update
    void Start()
    {
        Pelota = GameObject.FindGameObjectWithTag("Pelota");
        pelScript = Pelota.GetComponent<PelotaScript>();
        bumppala = GetComponent<AudioSource>();
        pala = GetComponent<Transform>();
        rgbd = GetComponent<Rigidbody>();
        rgbdPelota = Pelota.GetComponent<Rigidbody>();
        pelotatrans = Pelota.GetComponent<Transform>();
        estadoPower1 = estadosPowers.NoDisponible;
        estadoPower2 = estadosPowers.NoDisponible;
        currentTimePower = 0;
        currentTimePower2 = 0;
        Power1Act = false;
        Power2Act = false;
        Afuera = false;
        switch (IAdificultat)
        {
            case DificSelection.Facil:
                veloc = difVel[0];
                break;
            case DificSelection.Normal:
                veloc = difVel[1];
                break;
            case DificSelection.Dificil:
                veloc = difVel[2];
                break;
            default:
                veloc = difVel[0];
                break;
        }

        switch (IAdificultat)
        {
            case DificSelection.Facil:
                maxDif = maxDifeDif[0];
                break;
            case DificSelection.Normal:
                maxDif = maxDifeDif[1];
                break;
            case DificSelection.Dificil:
                maxDif = maxDifeDif[2];
                break;
            default:
                maxDif = maxDifeDif[0];
                break;
        }
        switch (IAdificultat)
        {
            case DificSelection.Facil:
                maxDis = maxDisDif[0];
                break;
            case DificSelection.Normal:
                maxDis = maxDisDif[1];
                break;
            case DificSelection.Dificil:
                maxDis = maxDisDif[2];
                break;
            default:
                maxDis = maxDisDif[0];
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {

        int pointsEn = GameManager.Instance.getPointsEnemy();
        int pointPl = GameManager.Instance.getPointsP1();

        diferenciaPuntos = pointPl - pointsEn;



        if(Pelota == null)
        {
            Pelota = GameObject.FindGameObjectWithTag("Pelota");
            pala = GetComponent<Transform>();
            rgbdPelota = Pelota.GetComponent<Rigidbody>();
            pelotatrans = Pelota.GetComponent<Transform>();
        }


            if(pelotatrans.position.y < 151 && pelotatrans.position.y > -152)
            { 
                if(pala.transform.position.x - pelotatrans.position.x < maxDis)
                { 
                    if (pala.position.y  > pelotatrans.position.y)
                    {
                        transform.position += new Vector3(0, -veloc, 0);
                    }
                    if (pala.position.y < pelotatrans.position.y )
                    {
                        transform.position += new Vector3(0, veloc, 0);
                    }
                    if (pala.position.y == pelotatrans.position.y)
                    {
                
                    }
                }
            };


        if (estadoPower1 == estadosPowers.Activado)
        {
            currentTimePower -= Time.deltaTime;
            
        }
        if (estadoPower1 == estadosPowers.Activado && currentTimePower <= 0)
        {
            Power1Act = false;
            estadoPower1 = estadosPowers.NoDisponible;
            currentTimePower = 0;
        }

        if (estadoPower1 == estadosPowers.Disponibe)
        {
            Debug.Log("Disponible PU");
           if(diferenciaPuntos >= maxDif)
            {
                if(Power2Act == false && Power2Act == false)
                {
                    Debug.Log("Perdiendo");
                    currentTimePower = timePowers;
                    Power1Act = true;
                    estadoPower1 = estadosPowers.Activado;
                }
                
            } 
        }

        if (estadoPower2 == estadosPowers.Activado)
        {
            currentTimePower2 -= Time.deltaTime;

        }
        if (estadoPower2 == estadosPowers.Activado && currentTimePower <= 0)
        {
            Power2Act = false;
            estadoPower2 = estadosPowers.NoDisponible;
            currentTimePower2 = 0;
        }

        if (estadoPower2 == estadosPowers.Disponibe)
        {
            if (diferenciaPuntos >= maxDif)
            {
                if(Power2Act == false && Power1Act == false)
                {
                    currentTimePower2 = timePowers;
                    Power2Act = true;
                    estadoPower2 = estadosPowers.Activado;
                }
                
            }
        }
    }
    void OnCollisionEnter(Collision col)
    {
        bumppala.Play();
        if (col.collider.tag == "Pelota")
        {

            foreach (ContactPoint contact in col.contacts)
            {

                rgbdPelota.AddForce(-1 * contact.normal.x * Potencia, -1 * contact.normal.y * Potencia, 0, ForceMode.Impulse);

            }


        }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PowerUp")
        {
            
                TipoPowerUps tipoCaja = col.gameObject.GetComponent<CajaPowerUp>().TipoPowerUp;
                switch (tipoCaja)
                {
                    case TipoPowerUps.Rebote:
                        if (estadoPower1 == estadosPowers.NoDisponible)
                        {
                            estadoPower1 = estadosPowers.Disponibe;
                        }
                        break;
                    case TipoPowerUps.MasPotencia:
                        if (estadoPower2 == estadosPowers.NoDisponible)
                        {
                            estadoPower2 = estadosPowers.Disponibe;
                        }
                        break;
                    default:
                        Debug.Log(tipoCaja);
                        break;
                }

            Destroy(col.gameObject);
            GameManager.Instance.getPU();
        }
    }

    public void setDificult(string dif)
    {
        if(dif == "Facil")
        {
            IAdificultat = DificSelection.Facil;
        }
        else if(dif == "Normal")
        {
            IAdificultat = DificSelection.Normal;
        }
        else
        {
            IAdificultat = DificSelection.Dificil;
        }
    }
    public float getTime()
    {
        return currentTimePower;
    }

    public float getTime2()
    {
        return currentTimePower2;
    }


    public estadosPowers getEstadoP1()
    {
        return estadoPower1;
    }

    public estadosPowers getEstadoP2()
    {
        return estadoPower2;
    }

    public bool getActivated()
    {
        return Power1Act;
    }

}
